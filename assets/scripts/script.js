const container = document.getElementById('print-screen');
const totalBill = document.getElementById('input-bill');
const friendName = document.getElementById('input-name');
const personalShare = document.getElementById('personal-share');
const computeBtn = document.getElementById('compute-button');

let totalPersonalShare = 0;
let counter = 0;
let friendsHolder = [];

function clearContainer(){
	container.lastElementChild.innerHTML = "";
}

computeBtn.addEventListener('click', function(){
	let billAmount = totalBill.value;
	let name = friendName.value;
	let share = personalShare.value;

	if(billAmount === "" || name === "" || share === ""){
		alert('Complete the form!')
	}

	else{
		let friend = {
			name: name,
			share: share,
			group: 0
		}

		clearContainer();

		friendsHolder[counter] = friend;
	
		totalPersonalShare += parseInt(share);

		friendsHolder[counter].name = name;
		friendsHolder[counter].share = share;
		
		for(let i = 0; i <= counter;i++){
			friendsHolder[i].group = (parseInt(billAmount) - totalPersonalShare)/(counter+1);
		}

		for(let j = 0;j <= counter;j++){
			let temp = document.createElement('h6');

			temp.textContent = friendsHolder[j].name + " = " + friendsHolder[j].share + " + " + friendsHolder[j].group; 
			container.lastElementChild.appendChild(temp);
		}
		counter++;
	}
});